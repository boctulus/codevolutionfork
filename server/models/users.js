const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: String,
    password: String
});

// The first argument is the singular name of the collection your model is for. Mongoose automatically looks for the plural version of your model name. 
// el tercer parametro ?
module.exports = mongoose.model('user', userSchema, 'users');