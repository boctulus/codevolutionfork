const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('./../models/users');
const mongoose = require('mongoose');

// https://mlab.com/databases/eventsdb/collections/users
const db = 'mongodb://boctulus:gogogo2k@ds135714.mlab.com:35714/eventsdb';  
const scret_key = 'clave screta!';

mongoose.connect(db, err => {
    if (err) {
        console.error('Error! ' + err);
    }else{
        console.log('Connected to MongoDb');
    }
});

// Middleware
function verifyToken(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send("Acceeso no-autorizado");
    }

    try {
        const token = req.headers.authorization.split(' ')[1];
        if (token === 'null') {
            return res.status(401).send("Acceeso no-autorizado");
        }
        const payload = jwt.verify(token,scret_key);
        if (!payload) {
            return res.status(401).send("Acceeso no-autorizado");
        }
        req.userId = payload.subject;
    } catch (e) {
        console.error('Error',e);
        return res.status(401).send("Acceeso no-autorizado");
    }

    next();
}

router.post('/register', (req, res) => {
    let userData = req.body;
    let user = new User(userData);
    user.save((error, registeredUser) => {
        if (error) {
            console.error(error);
        }else{
            let payload = { subject: registeredUser._id};
            let token = jwt.sign(payload, scret_key);
            res.status(200).send({token});
        }
    })
});

router.post('/login', (req, res) => {
    let userData = req.body;

    User.findOne({email: userData.email}, (error, user) => {
        if (error) {
            console.error('Error!!', error);
        }else{
            if (!user) {
                res.status(401).send('Invalid email');
            }else{
                if (user.password !== userData.password) {
                    res.status(401).send('Invalid password');
                }else{
                    let payload = { subject: user._id };
                    let token = jwt.sign(payload, scret_key);
                    res.status(200).send({token});
                }
            }
        }
    })
});

router.get('/',(req, res) => {
    res.send('Hello from router');
});

router.get('/events',(req, res) => {
    let events = getEvents(20);
    res.json(events);
});

// lo hago pasar por el Middleware verifyToken()
router.get('/special', verifyToken, (req, res) => {
    let events = getEvents(20);
    res.json(events);
});


// mock data
function getEvents(howmany){
    let event_arr = [];
    for(var i = 1; i <= howmany; i++){
     event_arr.push({
      "id" : i,
      "name" : "Auto Expo",
      "description" : "lorem ipsum",
      "date" : new Date()
     })
    }
    return event_arr;
   }

module.exports = router;